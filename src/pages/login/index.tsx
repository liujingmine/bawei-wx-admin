import styles from './index.less';
import {
  LoginForm,
  ProFormText,
} from '@ant-design/pro-components';
import { useState } from 'react';
import { v4 as uuidv4 } from "uuid"
const LoginPage = () => {
  const [codeImg, setCodeImg] = useState(() =>
    `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${uuidv4()}`
  )
  const changeCodeImg = async () => {
    setCodeImg(`https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${uuidv4()}`)
  }
  return (
    <div className="main">
    <LoginForm logo="" title={"八维电商管理平台"} >
      <div><ProFormText
        name="username"
        placeholder={'账号'}
        rules={[
          {
            required: true,
            message: '请输入用户名!',
          },
        ]}
      /></div>
      <div>
      <ProFormText.Password
        name="password"
        placeholder={'密码'}
        rules={[
          {
            required: true,
            message: '请输入密码！',
          },
        ]}
      />
      </div>
      <div className="style.yzmPass">
        <div>
        <ProFormText
          placeholder={'验证码'}
          name="captcha"
          rules={[
            {
              required: true,
              message: '请输入验证码！',
            },
          ]}
        />
        </div>
       <img src={codeImg} alt="" onClick={changeCodeImg}  className="style.imgs"/>
      </div>
    </LoginForm>
    </div>
  );
}
export default LoginPage;