import { defineConfig } from 'umi';
import routes from "./routes"

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  layout:{
    name: '八维电商平台',
    layout: 'side',
  },
  title:'八维电商管理平台',
  fastRefresh: {},
});
